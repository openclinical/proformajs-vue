This project is in **MAINTENANCE MODE** and the codebase has migrated to Vue 3.
* see https://gitlab.com/openclinical/proformajs-vue3
* see https://gitlab.com/openclinical/proformajs-vue/-/issues/14

# PROformajs vue components

Provides vue2 components (using bootstrap 4) for writing and reviewing proformajs protocols.

## getting Started

You'll need to have git and node (v12+) installed.

### run

```
you@yourpc:~$ git clone https://issues.openclinical.net/labs/proformajs-vue
you@yourpc:~$ cd proformajs-vue
you@yourpc:~/proformajs-vue$ npm install
you@yourpc:~/proformajs-vue$ npm run dev

  VITE v3.2.5  ready in 433 ms

  ➜  Local:   http://localhost:5173/
  ➜  Network: use --host to expose
```

### build

```
your@yourpc:~/proformajs-vue$ npm run build

> @openclinical/proformajs-vue@0.4.0 build
> vite build

vite v3.2.5 building for production...
transforming (104) src/lib-components/ReviewMapTask.vue
✓ 127 modules transformed.
dist/proformajs-vue.js   236.47 KiB / gzip: 48.00 KiB
dist/proformajs-vue.umd.cjs   197.84 KiB / gzip: 44.95 KiB
```

### use

https://gitlab.com/openclinical/proformajs-demo shows how you can use this project.

## notes

``npm init vue@2`` (v2.2.2) was used to generate this scaffold.

### TODO

- [x] vue ace json editor
  - [x] integrate - https://github.com/chairuosen/vue2-ace-editor
  - [x] enable search functionality
  - [x] fix syncing issue in tabbed demo
  - [x] sync cursor/highlight with selected task
- [ ] vue task map
  - [x] add decision, enquiry and plan components and make all sub-tasks draggable
  - [x] clickable working breadcrumb and clicking into sub plans
  - [x] create new tasks
  - [x] selectable tasks for component attribute editor
  - [x] draggable temporal dependencies
  - [x] create new temporal dependencies
  - [x] delete task
  - [x] delete temporal dependency
  - [x] show grabbable tasks with grab handle
  - [ ] move/delete groups of tasks
  - [ ] ctrl-c ctrl-v into sub plan
  - [ ] disallow dragging off canvas
  - [ ] allow user to change size of canvas
  - [ ] add panning and zooming
  - [x] handle protocol without svg positioning meta (i.e. generate it automatically)
  - [x] handle single task protocol
  - [x] show existence of data definitions/sources/candidates and sub-tasks on canvas
  - [ ] list available data-definitions to the selected task on the canvas?
  - [ ] indicate cyclic / autonomous / triggered / optional tasks on the map
- [x] vue protocol validation feedback component
- [ ] vue component attribute editor
  - [x] details editor
  - [ ] prevent names with incorrect characters
  - [ ] cascade name changes appropriately
  - [ ] data definitions editor
    - [x] use control to select data type and create a new data definition (named integer0, boolean1 etc)
    - [x] returning from Data Definition editor should go back to Data tab of the Task editor
    - [x] make range editable
      - [x] provide choice of raw or annotated until the first range value is entered, after which this control should be disabled
      - [x] add / edit / delete range values
      - [ ] check data types
    - [x] resetting the protocol should return the task editor to the Details tabe, no matter where we are
    - [x] provide mechanism to delete a data definition
    - [x] closing data definition dialogue should set index of the data definition dialogue back to 1 and rangeAnnotation to false and rangeIdx to -1
    - [x] show validation issues for defaultValue, valueCondition and range
    - [x] add help text to fields
    - [ ] CRUD Warnings
  - [x] sources editor
    - [x] CRUD sources
    - [x] custom requestCondition editor
  - [ ] candidates and arguments editor
    - [x] CRUD candidates
    - [x] CRUD arguments
    - [ ] custom recommendCondition editor
    - [ ] custom activeCondition editor
  - [x] pre/wait Conditions (which hide scheduling constraints)
  - [x] handle name change of schedule dependency
  - [x] remove data tab for non root plan
  - [ ] HTML editing for description
  - [x] cycle / event trigger attributes
- [ ] vue component tree component
  - [x] show clickable tree of components
  - [x] allow sources and candidates to select task
  - [ ] allow user to configure what is shown in tree
- [x] create npm package for vue components
- [x] favicon / title
- [ ] enactment review
  - [ ] add data
    - [x] single raw
    - [x] multi raw
    - [x] single ranged (optional annotations)
    - [x] multi ranged (optional annotations)
    - [x] Integer
    - [x] Float
    - [x] Text
    - [x] Boolean
    - [x] Date
    - [x] handle invalid data via text boxes
    - [x] fix reactivity issues
    - [x] dynamic data: disable input and show valueCondition
    - [ ] implement different view in enquiry and data
    - [ ] allow metadata to select widget (and implement range widget / buttongroup for select)
    - [ ] surface datadef warnings
  - [ ] allow cancel tasks / plans
  - [x] two column enquiry sources
  - [x] erase data
    - [ ] prevent erase for finished protocol
  - [x] draw decision
  - [x] confirm candidate
  - [x] view expressions/inactive arguments in decision
  - [x] view future and past tasks - prevent interaction in non in_progress tasks
  - [x] show enactment history
  - [x] show active tasks and triggers
  - [x] show existing data / history and allow update of data
  - [ ] allow custom enactment view, e.g. for shortness of breath
  - [ ] allow runtime expression evaluation in development mode
  - [ ] allow search/sort of data definitions in design and runtime interfaces
  - [x] restart enactment
  - [x] handle html descriptions with v-html
  - [ ] include proformajs-test
  - [ ] reduce delay when doing any action in a big guidelines, e.g. donor pathway
    - [ ] completing a task
    - [ ] tabbing between Sources
    - [ ] selecting a candidate
- [ ] code quality
  - [ ] ensure all props are typed

### Test plan

* Create new plan. Use the editor to edit plan attributes (name, caption, description, autonomous, cyclic). Check that edited attributes can be seen in the code view.  Edit attributes in the code view.  Check that the new edits can be seen in the editor view.
* Add an integer data definition: ok, "Are you ok?" with an annotated range of 1 - "Yes" and 0 - "No".  Add a text data definition: fruit, "What fruit do you like" with a range of fruit of your choice.  remove fruit. edit fruit. Check that your changes are reflected in the code.  Edit some of the fruit in the code.  Check that those changes are reflected in the editor.  Delete fruit items in code and editor.  Delete one of the data definitions.
* Create a new plan.  Add a new ``dob`` date data definition.  Also add a new ``age`` integer data definition with a valueCondtion of ``is_known('dob') ? now().diff(dob, 'years') : undefined``.  Add an enquiry that collects dob and then an action has "${age} years old" as its description.  When you review the pathway and enter a date you should see the correct current age shown on the next task.
* Create a nested set of tasks that includes every task type.  Navigate around using the breadcrumb to return to the root task.  Check that you can move tasks around.  Join the tasks with arrows (temporal constraints).  Remove tasks with upstream and downstream constraints and check that the arrows are removed too.
* Start with a new plan.  add a new enquiry and plan. double click on plan and add a decision.  click on the root plan in the breadcrumb.  You should see two issues (missing sources and candidates).  If you click on the decision path it should be selected and the parent plan changed in the map.
* Start with a new plan.  Add a new ``age`` data definition with the caption "What is your age?".  Add an enquiry that sources that data definition and make it required.  Then add a new decision and join the enquiry and the decision.  Add a ``yes`` and a ``no`` candidate for that decision.  Set the recommendation condition for yes to be "age>=18" and for no to be "age<18". Make the decision automatic.  Then drop an action on the canvas and connect the decision to the action.  Set the description to be "You ${result_of('decisionB')=="yes" ? "can" : "cannot"} vote". When run with an age of 17 the action should say you cannot vote and when run with an age of 18 the action should say you can vote.
* Start a new plan.  Add five data definitions: credit_history (Text - range: {"good"/"bad"}), loan_amount (int), loan_term (int range: {12,36,60}), applicant_income (int), coapplicant_income (int), total_income (integer with a value condition of ``applicant_income + (is_known("coapplicant_income") ?coapplicant_income : 0)``). Add an enquiry (named enquire) to source the first five definitions, making all but ``coapplicant_income`` required.  Add a decision (named decide) with two candidates ``grant`` and ``deny``.  For grant add recommendCondition ``net_support("grant")>1`` and the argumentation:
  + ``loan_amount<0.40*total_income*loan_term/60``
  + ``credit_history=="good"``
  - ``credit_history=="bad"``
For deny add the recommendCondition ``net_support("grant")<=1``.  Add two actions, one called "granted" and the other called "denied". For the granted action add the preCondition ``result_of('decide')=="grant"``. For the denied action add the preCondition ``result_of('decide')=="deny"``.  An application for a loan of 1000 to be repaid over 12 months by an applicant with an income of 10000 should be denied.  If you add a coapplicant with an income of 30000 it should be granted.
* Cut and paste a new protocol (e.g. [one from proformajs](https://gitlab.com/openclinical/proformajs/-/blob/main/etc/assess.json)) into the code view.  Check that new protocol is visible in the editor and that the root plan is selected.
* Create a new plan. add a new action. make it cyclic with a cycleUntil of "index('plan:actionA')==2" and a caption of "Task A ${index('plan:actionA')}".  Review an enactment.  In the history you should see "Task A 0", "Task A 1" and "Task A 2"

### Issues

#### task editor

- [x] adding a space to a task name should be handled better
- [ ] fix tabIndex issues:
  -  [ ] a new enquiry opens on the sources tab when there are no other sub tasks
  -  [ ] a new enquiry opens on the constraints tab when there other sub tasks
- [ ] changing a name should change references to that name too and (maybe) automatically update the caption
- [ ] deleting a task in a sub-plan moves focus to root plan (should move to the parent)

#### code editor

- [x] cant edit name attribute - the editor reacts by removing attribute all together

#### component tree

- [x] needs to be open automatically

#### cyclic tasks

more work needed here.  a single cyclic task limited to two iterations that shows the index in the caption ...

- [x] cycleUntil example needs updating
- [x] review map doesnt show task as finished at the end (maybe it isnt?)
- [x] review map shows raw caption and not the runtime caption
- [x] review history shows three "Task B (2)" where it should show "Task B (0)" then "Task B (1)" then "Task B (2)"

#### selection

- [x] selected task isnt highlighted when walking through an enactment

#### enquiry

- [x] adding an annotated range to gender throws up two issues:  
  - [x] value is added as string not integer
  - [x] values are shown as json

#### mobile view

- [x] needs work

#### touch screen issues

- [x] cannot see a task being dragged (ipad safari / edge)
- [x] cannot descend by double tapping into sub plan in compose view (ipad safari)
- [x] ipad automatically capitalises the first letter for data definitions / candidates (ipad)
- [x] cannot open/close tree view node
- [x] cannot delete task
- [x] cannot delete temporal constraint
- [x] cannot search in the code view

#### review

- [x] map not synced with selected task
- [x] cannot descend into sub plan from review map
- [x] single task view (decision) doesnt need to show parent caption and description
- [ ] integers should have integer data entry boxes etc
- [ ] disable the select tab for a completed enactment
- [x] needs to show html content

## Licensing

PROformajs is owned by Openclinical CIC and dual-licensed with GPLv3 and a commercial license (please email licensing@openclinical.net for more details).  Contributions to the project are very welcome, and will be recognised via the contrib file, but will need to have copyright assigned to the CIC.
