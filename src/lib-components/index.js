/* NB removed :: eslint-disable import/prefer-default-export */
import Compose from './Compose.vue'
import ComposeTask from './ComposeTask.vue'
import ComposeTree from './ComposeTree.vue'
import ComposeCode from './ComposeCode.vue'
import ComposeReview from './ComposeReview.vue'
import ComposeMap from './ComposeMap.vue'
import ReviewMarkdown from './ReviewMarkdown.vue'

export {
  Compose,
  ComposeMap,
  ComposeTask,
  ComposeTree,
  ComposeCode,
  ComposeReview,
  ReviewMarkdown
}
