import { fileURLToPath, URL } from "node:url";

import { resolve } from 'path';
import { defineConfig } from "vite";
import vue2 from "@vitejs/plugin-vue2";

// see https://github.com/vitejs/vite/issues/1579
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';

// see https://stackoverflow.com/questions/74763160/how-to-make-vite-ignore-docs-blocks
const vueDocsPlugin = {
  name: 'vue-docs',
  transform(_code, id) {
    if(!/vue&type=docs/.test(id)) return
    return `export default ''`
  }
}

// https://vitejs.dev/config/
export default defineConfig({
  esbuild: {
    keepNames: true
  },
  optimizeDeps: {
    esbuildOptions: {
      keepNames: true,
    },
  },
  plugins: [
    vue2(),
    vueDocsPlugin,
    cssInjectedByJsPlugin()
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    lib: {
      // Could also be a dictionary or array of multiple entry points
      entry: resolve(__dirname, 'src/entry.js'),
      name: '@openclinical/proformajs-vue',
      // the proper extensions will be added
      fileName: 'proformajs-vue',
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue', '@openclinical/proformajs', 'vue2-ace-editor', 'moment', 'file-saver', 'marked', 'dompurify', 'vue2-hammer', '@fortawesome/fontawesome-svg-core', '@fortawesome/free-solid-svg-icons', '@fortawesome/vue-fontawesome'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
          'vue2-ace-editor': 'AceEditor',
          moment: 'moment',
          'file-saver': 'FileSaver',
          dompurify: 'createDOMPurify',
          marked: 'marked',
          '@openclinical/proformajs': 'proformajs',
          'vue2-hammer': 'VueHammer',
          '@fortawesome/fontawesome-svg-core': 'library',
          '@fortawesome/free-solid-svg-icons': 'FontAwesomeIcons',
          '@fortawesome/vue-fontawesome': 'FontAwesomeIcon'
        },
      },
    }
  },
});
